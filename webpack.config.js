const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: "development",
  devtool: "cheap-module-source-map",
  entry: ['babel-polyfill', "./src/js/index.js", "./src/scss/main.scss"],
  output: {
    path: path.resolve(__dirname, "dist", ),
    filename: "js/app.js",
    publicPath: "/assets/"
  },
  module: {
    rules: [
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2|otf|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({ // define where to save the file
      filename: 'css/[name].css',
      chunkFilename: "css/[id].css"
    }),
  ]

}