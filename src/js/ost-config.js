export const config = {
    apiKey: "68dcabbfcf3e49566585",
    secretKey: "6d7010007b8ad509aca1f07672b727a84070154395c7834f6424ac484b51b64e",
    endpoint: "https://cors-anywhere.herokuapp.com/https://sandboxapi.ost.com/v1.1"
}

export const directions = {
    userToUser: 1,
    companyToUser: 2,
    userToCompany: 3
}

export const actions = {
    like: {
        id: 39022,
        direction: directions.userToUser
    },
    thanks: {
        id: 39021,
        direction: directions.userToUser
    },
    reward: {
        id: 39020,
        direction: directions.companyToUser
    },
    post: {
        id: 39019,
        direction: directions.userToCompany
    }
}
export const actionsById = {
    39022: "like",
    39020: "reward",
    39019: "post"
}

export const errors = {
    INSUFFICIENT_FUNDS: "INSUFFICIENT_FUNDS"
}