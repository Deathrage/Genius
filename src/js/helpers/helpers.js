export default class Helpers {
    static ostSafeName(name) {
        return name.replace(/[\W_]+/g, '');
    }
    static getKeyByValue(object, value) {
        return Object.keys(object).find(key => object[key] == value);
    }
}