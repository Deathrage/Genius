import $ from "jquery";
import Helpers from "./helpers.js";

export default class Validators {
    static async validateEmail(email) {
        if (email.indexOf("@") == -1) {
            return "Email must include @.";
        } else if (Helpers.ostSafeName(email.split("@")[0]).length > 20 || Helpers.ostSafeName(email.split("@")[0]).length < 3) {
            return "Part before @ must must be a minimum of 3 characters and maximum of 20 characters withou special characters.";
        } 
    }
    static validateUserName(name) {
        if (name.length > 20 || name.length < 3) {
            return "Must be a minimum of 3 characters and maximum of 20 characters withou special characters.";
        }
    }
    static maxLength(input) {
        input = input || "";
        if (input.length > 300) {
            return "Must be at maximum 300 characters long.";
        }
    }
}