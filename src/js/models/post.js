export default class Post {
    constructor() {
        this.actor = null;
        this.object = null;
        this.target = null;
        this.verb = "post";
        //this.time = new Date(Date.now()).toUTCString();
        this.to = null;
        this.foreign_id = null;
        this.userName = null;
        this.title = null;
        this.message = null;
        this.image = null;
        this.likeCount = 0;
    }

    isValid() {
        let valid = true;
        return valid;
    }
}