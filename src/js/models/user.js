import firebase from "firebase";

import UserController from "../controllers/UserController.js";
import DatabaseController from "../controllers/DatabaseController.js";
import OstController from "../controllers/OstController.js";



export default class User {
    /**
     * @param  {firebase.User} firebaseUser
     */
    constructor(firebaseUser) {
        this.firebaseUser = firebaseUser;
        this.followers = null;
        this.following = null;
        this.userInfo = new UserInfo();
    }
    
    async populateUserInfo() {
        await this.userInfo.populate(this.firebaseUser.uid);
    }

    async ostAddress() {
        return await OstController.getAdress(this.userInfo.ostuuid);
    }

    async ostUUID() {
        let userInfo = await DatabaseController.getUserInfo(this.firebaseUser.uid);
        if (userInfo == null) {
            return null;
        }
        return userInfo["ostuuid"];
    }
    async balance() {
        return await OstController.getUserBalance(await this.ostUUID());
    }
}

export class UserInfo {
    constructor() {
        this.ostuuid = null;
        this.username = null;
        this.favoriteIdea = null;
        this.profilePic = null;
        this.location = null;
    }

    async populate(firebaseUI) {
        let self = this;
        await DatabaseController.getUserInfo(firebaseUI).then((infoObject)=>{
            Object.assign(self, infoObject);
        });
    }
}