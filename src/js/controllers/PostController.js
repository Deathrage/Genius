import stream from 'getstream';
import {config as streamConfig} from "../stream-config.js";
import Post from "../models/post.js";
import Like from "../models/like.js";

import OstController from "./OstController.js";
import {actions, directions, errors} from "../ost-config.js";
import {currentUser} from "../index.js";
import DatabaseController from './DatabaseController.js';

const client = stream.connect(streamConfig.key, streamConfig.secretKey, '38152');

export default class PostController {
    /**
     * @param  {Post} post
     */
    static async post(message, title, pic) {
        let er;
        let curFeed = client.feed("user", currentUser.firebaseUser.uid);
        //create post
        let post = new Post();
        post.actor = currentUser.firebaseUser.uid;
        post.userName = currentUser.userInfo.username || currentUser.firebaseUser.email;
        post.message = message;
        post.title = title;
        post.image = pic;

        let action = actions["post"];
        //validate post
        let isPostValid = post.isValid();
        if (action == null || isPostValid == false) {
            throw new Error("Invalid post or action doesnt match any");
        }
        //do ost action
        let id = await OstController.executeAction(action["id"], currentUser.userInfo.ostuuid, await OstController.companyUUID());
        //if insuficient funds, raise it
        if (id == errors.INSUFFICIENT_FUNDS) {
            er ="Insuficcient funds!";
            return er;
        }

        //fill necessary info to post from OST action
        post.object = id;
        post.foreign_id = id;
        //add post to feed
        curFeed.addActivity(post).then((data)=>{
            console.log(data);
        }).catch((err)=>{
            throw new Error(err);
        });
    }

    static async like(targetActionId, targetUserUuid) {
        let er;
        let likeFeed = client.feed("like", currentUser.firebaseUser.uid);
        let like = new Like();

        like.actor = currentUser.firebaseUser.uid;
        like.userName = currentUser.userInfo.username || currentUser.firebaseUser.email;
        like.to = ["notification:" + targetUserUuid];

        //ost
        let action = actions["like"];
        let targetOstUuid = (await DatabaseController.getUserInfo(targetUserUuid))["ostuuid"];
        let id = await OstController.executeAction(action["id"], currentUser.userInfo.ostuuid, targetOstUuid);
        //if insuficient funds, raise it
        if (id == errors.INSUFFICIENT_FUNDS) {
            er ="Insuficcient funds!";
            return er;
        }

        like.foreign_id = id;
        like.object = id;

        //update old acitivity
        let targetFeed = client.feed("user", targetUserUuid);
        await targetFeed.get({id_gte: targetActionId, id_lte:targetActionId}).then(async (data)=>{
            
            let results = data["results"];
            if (results.length == 0) {
              return;  
            }
            //get edited activity
            let activity = results[0];
            //remove old
            await targetFeed.removeActivity(targetActionId);
            //update 
            activity["likeCount"] = activity["likeCount"] + 1;
            //return it back updated
            let newId = (await targetFeed.addActivity(activity))["id"];
            //set new id as target
            like.target = newId;
            //add like to like feed
            await likeFeed.addActivity(like).then((data)=>{
                console.log(data);
            }).catch((err)=>{
                throw new Error(err);
            });
        });


    }
    
    static async usersFeed(userUUID, limit, offset) {
        let feed;
        let curFeed = client.feed("user", userUUID);
        await curFeed.get({limit: limit, offset: offset}).then((data)=>{
            feed = data["results"];
        }).catch((err)=>{
            throw new Error(err);
        });
        return feed;
    }

    static async myFeed(offset, limit) {
        return await this.usersFeed(currentUser.firebaseUser.uid, limit, offset);
    }

    static async getActivities() {
        let activities = await client.getActivities();
        console.log(activities);
    }

}