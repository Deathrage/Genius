import $ from "jquery";
import PostController from "./PostController";
import FollowerController from "./FollowController";
import User from "../models/user.js";
import DatabaseController from "./DatabaseController";
import UserController from "./UserController";
import LikeController from "./LikeController";
import Helpers from "../helpers/helpers.js";
import {actionsById} from "../ost-config.js";

export default class RenderController {

    static async renderFeed(target, feed, myuuid) {
        let $target = $(target);
        let $template = $(".rn-post-template ");
        let result = "";

        if (!(feed instanceof Array)) {
            feed = [feed];
        }
        for (let elT in feed) {
            let el = feed[elT];
            let $post = $template.clone(false);

            let userInfo = await DatabaseController.getUserInfo(el["actor"]);

            let myLikedActions = await LikeController.likedActions(myuuid);
            let didILike = await LikeController.didLike(el["id"], myLikedActions);

            if (didILike == false && el["actor"] != myuuid) {
                $post.find(".js-post-like").addClass("notliked");
            } else if (el["actor"] == myuuid) {
                $post.find(".js-post-like").addClass("off");
            }

            $post.attr("data-action-id", el["id"]);
            $post.attr("data-actor-id", el["actor"]);
            $post.attr("data-foreign-id", el["foreign_id"]);
            $post.find(".rn-unfollow").attr("data-user-id", el["actor"]);
            $post.find(".rn-post-imeage").attr("src", el["image"]);
            $post.find(".rn-post-profile-pic").attr("src", userInfo["profilePic"]);
            $post.find(".rn-post-username").text(userInfo["username"] || userInfo["usename"]);
            $post.find(".rn-post-title").text(el["title"]);
            $post.find(".rn-post-time").text((new Date(el["time"])).toUTCString());
            $post.find(".rn-post-message").text(el["message"]);
            $post.find(".rn-post-like-count").text(el["likeCount"]);
            $post.show();

            result +=  $post[0].outerHTML;
        }

        $target.append(result);
    }

    static async rednerUsers(target, users) {
        let $target = $(target);
        let $template = $(".rn-person-template");
        let result = "";
        let following = await FollowerController.myFollowing();


        for(let key in users) {
            let $post = $template.clone(false);

            $post.attr("data-user-id", key);
            $post.attr("data-username", users[key]["username"]);
            $post.find(".rn-people-profile-pic").attr("src", users[key]["profilePic"]);
            $post.find(".rn-people-username").text(users[key]["username"]);

            let isFollowing = FollowerController.isFollowing(following, key);
            if (isFollowing == true) {
                $post.find(".js-action").addClass("unfollow");
            } else {
                $post.find(".js-action").addClass("follow");
            }

            $post.show();

            // $post.find(".js-username").text(users[key]["username"]);
            // let $action = $post.find(".js-action");
            // $action.attr("data-id", key);

            //let isFollowing = FollowerController.isFollowing(following, key);
            // if (isFollowing == true) {
            //     $action.addClass("unfollow");
            //     $action.text("unfollow");
            // } else {
            //     $action.addClass("follow");
            //     $action.text("follow");
            // }

            // $post.show();
            
            result +=  $post[0].outerHTML;
        }

        $target.append(result);
    }
    /**
     * @param  {User} currentUser
     */
    static async fillUserInfoForm(currentUser) {
        let userinfo = await DatabaseController.getUserInfo(currentUser.firebaseUser.uid);
        if (userinfo == null) {
            this.errorMessage("There has been an error. Please contact support.");
            return;
        }
        console.log(userinfo);
        $("input[name='username']").val(userinfo["username"] || userinfo["usename"]);
        $("input[name='location']").val(userinfo["location"]);
        $("input[name='favoriteidea']").val(userinfo["favoriteIdea"]);
        $("input[name='profilepic']").val(userinfo["profilePic"]);
    }

    static evaluateLogin(loggedIn = false) {
        if (loggedIn == true) {
            $("body").addClass("logged");
        } else {
            $("body").removeClass("logged");
        }
    }

    static renderBalance(balance) {
        $(".rn-balance").text(balance);
    }
    static renderUsername(username) {
        $(".rn-username").text(username);
    }
    static renderProfilePic(pic) {
        $(".rn-profilepic").attr("src", pic);
    }
    static renderUsdBalance(usdBalance) {
        $(".rn-usd-balance").text(usdBalance);
    }
    static renderToElement(selector, value) {
        $(selector).text(value);
    }
    static renderToInput(selector, value) {
        $(selector).val(value);
    }

    static renderTransactions(list, myid) {
        let $target = $(".rn-transactions");
        let res = "";
        list.forEach((el)=>{
            let dir = el["to_user_id"] == myid ? "incre" : "decre";
            let action = actionsById[el["action_id"]];
            res += "<tr class=\""+dir+"\"><td>" + (new Date(el["timestamp"])).toUTCString() + "</td><td>" + el["from_user_id"] + "</td><td>" + el["to_user_id"] + "</td><td>" + action + "</td><td>" + (Number(el["amount"]).toFixed(2)) + "</td></tr>";
        });
        $target.html(res);
    }

    static errorMessage(msg) {
        $(".js-error-message").text(msg);
    }
    static successMessage(msg) {
        $(".js-success-message").text(msg);
    }
    static warningMessage(msg) {
        $(".js-warning-message").text(msg);
    }
    static emptyMessages() {
        $(".js-error-message").text("");
        $(".js-success-message").text("");
        $(".js-warning-message").text("");
    }
}