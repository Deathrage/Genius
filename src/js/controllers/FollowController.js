import stream from 'getstream';
import {config as streamConfig} from "../stream-config.js";
import Post from "../models/post.js";

import OstController from "./OstController.js";
import {actions, directions, errors} from "../ost-config.js";
import {currentUser} from "../index.js";

const client = stream.connect(streamConfig.key, streamConfig.secretKey, '38152');


export default class FollowerController {
    static async getFollowing(firebaseUUID) {
        let result;
        let curFeed = client.feed("user", firebaseUUID);
        await curFeed.following().then((data)=>{
            result = data["results"];
        })
        return result;
    }

    static async myFollowing() {
        return await this.getFollowing(currentUser.firebaseUser.uid);
    }

    static async getFollowers(firebaseUUID) {
        let result;
        let curFeed = client.feed("user",firebaseUUID);
        await curFeed.followers().then((data)=>{
            result = data["results"];
        })
        return result;
    }

    static async myFollowers() {
        return await this.getFollowers(currentUser.firebaseUser.uid);
    }

    static async follow(followedUID) {
        let curFeed = client.feed("user", currentUser.firebaseUser.uid);
        curFeed.follow('user', followedUID);

    }

    static async unfollow(followedUID) {
        let curFeed = client.feed("user", currentUser.firebaseUser.uid);
        curFeed.unfollow('user', followedUID);
    }

    static isFollowing(followerList, firebaseUID) {
        if (!(followerList instanceof Array)){
            followerList = [followerList];
        }
        let result = followerList.filter((el)=>{
            let id = el["target_id"].split(":")[1];
            return id == firebaseUID;
        });
        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    }
}