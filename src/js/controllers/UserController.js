import firebase from "firebase";
import logFirebaseError from "../helpers/logger.js";
import {LoginRequest} from '../models/requests.js';
import User from "../models/user.js";
import DatabaseController from "./DatabaseController.js";



export default class UserController {
    /**
     * @param  {LoginRequest} req
     */
    static async createFirebaseUser(req) {
        let er;
        await firebase.auth().createUserWithEmailAndPassword(req.username, req.password).catch((err)=>{
            er = err["message"];
        });
        return er;
    }
    /**
     * @param  {LoginRequest} req
     * @returns {firebase.User}
     */
    static async signIn(req) {
        let er;
        await firebase.auth().signInWithEmailAndPassword(req.username, req.password).catch((err)=>{
            er = err["message"];
        });
        return er;
    }
    static async signOut() {
        if (this.currentUser != null) {
            firebase.auth().signOut()
        }
    }
    static get currentUser() {
        return new User(firebase.auth().currentUser);
    }
}