export default class Router {

    static restrictRedirect(loggedIn = false) {
        if (loggedIn == true) {
            if (!(loggedPaths.indexOf(this.location) > -1)) {
                this.feed();
            }
        } else {
            if (!(unloggedPaths.indexOf(this.location) > -1)) {
                this.homepage();
            }
        }
    }

    static homepage() {
        window.location.href = "/";
    }
    static feed() {
        window.location.href = "/feed.html";
    }

    static get location() {
        return window.location.pathname;
    }
}

const unloggedPaths = [
    "",
    "/",
    "/index.html",
    "/signin.html",
    "/signup.html"
];
const loggedPaths = [
    "/feed.html",
    "/profile.html",
    "/wallet.html"
]