import firebase from "firebase";
import UserController from "./UserController.js";


export default class DatabaseController {

    //create new link record
    static async createUserInfo(firebaseUUID, ostUUID, username, favoriteIdea, location, profilePic) {
        let er;
        await firebase.database().ref('users/' + firebaseUUID).set({
            ostuuid: ostUUID || "",
            username: username || "",
            favoriteIdea: favoriteIdea || "",
            location: location || "",
            profilePic: profilePic || ""
        }).catch((err)=>{
            er = err["message"];
        });
        return er;
    }
    //get link recrod by firebase uuid
    static async getUserInfo(firebaseUUID) {
        let snapshot;
        await firebase.database().ref("users/" + firebaseUUID).once("value").then((data)=>{
            snapshot = data.val();//["node_"]["value_"];
        });
        return snapshot;
    }
    //remove link
    static async removeLink(firebaseUUID) {
        await firebase.database().ref("users/" + firebaseUUID).ref.remove();
    }

    static async getAllUsers(me) {
        let snapshot;
        await firebase.database().ref("users").once("value").then((data)=>{
            snapshot = data.val();//["node_"]["value_"];
        });
        if (me) {
            delete snapshot[me];
        }
        return snapshot;
    }
}