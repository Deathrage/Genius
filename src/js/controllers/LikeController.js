import stream from 'getstream';
import {config as streamConfig} from "../stream-config.js";
import Post from "../models/post.js";

import OstController from "./OstController.js";
import {actions, directions, errors} from "../ost-config.js";
import {currentUser} from "../index.js";

const client = stream.connect(streamConfig.key, streamConfig.secretKey, '38152');


export default class LikeController {
    /**
     * @param  {} actionId
     * @param  {Array} likedActions
     */
    static async didLike(actionId, likedActions) {
        let didILike = false;
        let res = likedActions.filter((el)=>{
            return el["target"] == actionId;
        });
        if (res.length > 0) {
            didILike = true;
        }
        return didILike;
    }

    static async likedActions(userUuid) {
        let likeFeed = client.feed("like", userUuid);
        let likedActions;
        await likeFeed.get().then(data => {
            likedActions= data["results"];
        });
        return likedActions;
    }

}