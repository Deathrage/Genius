import firebase from "firebase";
import User from "../models/user.js";
import DatabaseController from "./DatabaseController.js";

//ost
import OSTSDK from "@ostdotcom/ost-sdk-js";
import {config as ostConfig, errors} from "../ost-config.js";
import RenderController from "./RenderController.js";


export const ostObj = new OSTSDK({apiKey: ostConfig.apiKey, apiSecret: ostConfig.secretKey, apiEndpoint: ostConfig.endpoint});
export const userService = ostObj.services.users;
export const airdropService = ostObj.services.airdrops;
export const balanceService = ostObj.services.balances;
export const transactionService = ostObj.services.transactions;
export const tokenService = ostObj.services.token;
export const ledgerService = ostObj.services.ledger;

export default class OstController {
    /**
     * @param  {User} firebaseUser
     */
    static async enforceOst(firebaseUser) {
        //if none is found create new
        if (await firebaseUser.ostUUID() == null) {
            let ostId  = await this.createUser(firebaseUser); //create ost kit
            await DatabaseController.createUserInfo(firebaseUser.firebaseUser.uid, ostId, firebaseUser.firebaseUser.email).catch(()=>{
                RenderController.errorMessage("There has been an issue with database. Please contact support. Do not close this window.");
            }); //create link
            RenderController.successMessage("Your GEN account was established. In a short time you will recieve your first tokens.")
        }
    }
    /**
     * @param  {User} firebaseUser
     * @returns {Number} 
     */
    static async createUser(firebaseUser) {
        let id;
        await userService.create({name: firebaseUser.firebaseUser.email.split("@")[0].replace(".","")}).then((reason)=>{
            id = reason.data.user.id;
        }).catch((er)=>{
            RenderController.errorMessage("There has been an issue with GEN. Please contact support. Do not close this window.");
            console.log(er);
        });
        //aidrdrop new user 10 tokens
        await this.airdrop(10, id);
        return id;
    }

    static async airdrop(amount, ostUUID) {
        let id
        await airdropService.execute({amount: amount, user_ids: ostUUID}).then((reason)=>{
            id = reason.data["airdrop"]["id"];
        }).catch((er)=>{
            RenderController.errorMessage("There has been an issue with GEN. Please contact support. Do not close this window.");
            console.log(er);
        });
        return id;
    }

    static async getUserBalance(ostUUID) {
        let balance;
        await balanceService.get({id: ostUUID}).then(function(res) { 
            balance = res.data.balance["available_balance"];
        }).catch((er)=>{
            RenderController.errorMessage("There has been an issue with GEN. Please contact support. Do not close this window.");
            console.log(er);
        });
        return balance;
    }

    static async executeAction(id, from, to) {
        let tId;
        await transactionService.execute({from_user_id: from, to_user_id: to, action_id: id}).then(function(res) { 
            tId = res['data']['transaction']['id'];
        }).catch(function(er) {
            let erCode = er["err"]["code"];
            if (erCode == errors.INSUFFICIENT_FUNDS) {
                tId = erCode;
            } else {
                RenderController.errorMessage("There has been an issue with GEN. Please contact support. Do not close this window.");
                console.log(er);
            }
        });
        return tId;
    }

    static async tokenService() {
        let data
        await tokenService.get({}).then(function(res) { 
            data = res;
        }).catch(function(err) { 
            throw new Error(err)
        });
        return data;
    }
    static async companyUUID(){
        let data = await this.tokenService();
        return data["data"]["token"]["company_uuid"];
    }
    static async exchangeRate(){
        let data = await this.tokenService();
        return Number(data["data"]["price_points"]["OST"]["USD"]);
    }

    static async getAdress(ostuuid) {
        let adress;
        await userService.get({id: ostuuid}).then(function(res) {
            let adresses = res["data"]["user"]["addresses"];

            adress = adresses[0][1];

            }).catch(function(err) { 
                throw new Error(err);
            });
        return adress;
    }

    static async getLedger(ostuuid) {
        let ledger;
        await ledgerService.get({id: ostuuid}).then(function(res) { 
            ledger = res["data"]["transactions"];
        }).catch(function(err) { 
            throw new Error(err);
        });
        return ledger;
    }

}
