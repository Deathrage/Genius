//firebase
import firebase from "firebase";
import {config as firebaseConfig} from "./firebase-config.js";


//jquery
import $ from "jquery";

//controllers
import UserController from "./controllers/UserController.js";
import OstController from "./controllers/OstController.js";


//models
import {LoginRequest} from './models/requests.js';
import User from "./models/user.js";
import DatabaseController from './controllers/DatabaseController.js';
import PostController from "./controllers/PostController.js";
import RenderController from "./controllers/RenderController.js";
import FollowerController from "./controllers/FollowController.js";
import Router from "./controllers/RouterContoller.js";
import Helpers from "./helpers/helpers.js";
import Validators from "./helpers/validators.js";


//start firebase 
firebase.initializeApp(firebaseConfig);

export let currentUser= new User();

let feedLimit = 10;

$(function(){
    firebase.auth().onAuthStateChanged(async (user) => {
        if (user) {
            //check restriction
            Router.restrictRedirect(true);

            RenderController.evaluateLogin(true);
            //populate user and user info
            currentUser = new User(user);
            await OstController.enforceOst(currentUser);
            await currentUser.populateUserInfo();

            RenderController.renderUsername(currentUser.userInfo.username || currentUser.firebaseUser.email);
            RenderController.renderProfilePic(currentUser.userInfo.profilePic);
            RenderController.renderToInput(".rn-ost-id", currentUser.userInfo.ostuuid);


            currentUser.ostAddress().then(adress =>{
                RenderController.renderToInput(".rn-ost-adress", adress);
            });

            currentUser.balance().then(async (data)=>{
                RenderController.renderBalance(Number(data).toFixed(2));
                let usdBal = await OstController.exchangeRate();
                usdBal = Number(usdBal) * Number(data).toFixed(2);
                RenderController.renderUsdBalance(Number(usdBal).toFixed(2));
            });

            //populate info by pages
            if (Router.location == "/profile.html") {
                RenderController.fillUserInfoForm(currentUser);
            }
            if (Router.location == "/feed.html") {
                renderFeed(0, feedLimit);
                renderUsers(currentUser);
            }
            if (Router.location == "/wallet.html") {
                OstController.getLedger(currentUser.userInfo.ostuuid).then(async (data)=>{
                    RenderController.renderTransactions(data, currentUser.userInfo.ostuuid);
                });
            }

            //followers and followings 
            currentUser.following = await FollowerController.myFollowing();
            currentUser.followers = await FollowerController.myFollowers();

            console.log(currentUser);

            //register control events for logged user
            unbindEvents();
            eventsLogged();
            eventsCommon();
        } else {
            //check restriction
            Router.restrictRedirect(false);

            RenderController.evaluateLogin(false);

            // $(".js-curus").text("");
            // $(".js-firebase").text("");
            // $(".js-ost").text("");
            // $(".js-balance").text("");
            
            //register event for not logged user
            unbindEvents();
            eventUnlogged();
            eventsCommon();
        }
    });

    // $(".js-create").on("click", "input[type='button']", (e)=>{
    //     let req = new LoginRequest();
    //     req.username = $(e.currentTarget).siblings("input[name='username']").val();
    //     req.passwrod = $(e.currentTarget).siblings("input[name='password']").val();

    //     UserController.createFirebaseUser(req);
    // });
    
    // $(".js-login").on("click", "input[type='button']", (e)=>{
    //     let req = new LoginRequest();
    //     req.username = $(e.currentTarget).siblings("input[name='username']").val();
    //     req.passwrod = $(e.currentTarget).siblings("input[name='password']").val();

    //     UserController.signIn(req);
    // });

    // $(".js-logout").on("click", (e)=>{
    //     UserController.signOut();
    // });

    // $(".js-post").on("click", (e)=>{
    //     let msg = $(e.currentTarget).siblings("input[type='text']").val();
    //     PostController.post(msg);
    // });

    // $("body").on("click", ".js-action.follow", (e)=>{
    //     let $target = $(e.currentTarget);
    //     let userUuid = $target.attr("data-id");
    //     FollowerController.follow(userUuid);
    //     console.log("User " + currentUser.firebaseUser.uid + " is following " + userUuid);
    // });

    // $("body").on("click", ".js-action.unfollow", (e)=>{
    //     let $target = $(e.currentTarget);
    //     let userUuid = $target.attr("data-id");
    //     FollowerController.unfollow(userUuid);
    //     console.log("User " + currentUser.firebaseUser.uid + " is not following " + userUuid + " anymore");
    // });
});

function eventsCommon() {
    
}

function eventsLogged() {
    $("body").on("click", ".js-signout", (e)=>{
        UserController.signOut();
        Router.homepage();
    });
    $(".js-signout").on("click", ()=>{
        UserController.signOut();
        Router.homepage();
    });
    //update user info
    $(".js-aditionaluserinfo-form").on("click", ".js-set", async (e)=>{
        let username = $(".js-aditionaluserinfo-form").find("input[name='username']").val();
        let location = $(".js-aditionaluserinfo-form").find("input[name='location']").val();
        let favoriteidea = $(".js-aditionaluserinfo-form").find("input[name='favoriteidea']").val();
        let profilepic = $(".js-aditionaluserinfo-form").find("input[name='profilepic']").val();
        let er = Validators.maxLength(username) || Validators.maxLength(location) || Validators.maxLength(favoriteidea) || Validators.maxLength(profilepic);
        if (er == null) {
            er = await DatabaseController.createUserInfo(currentUser.firebaseUser.uid, currentUser.userInfo.ostuuid, username, favoriteidea, location, profilepic);
        }
        if (er != null) {
            RenderController.emptyMessages();
            $(".js-aditionaluserinfo-form .js-error").text(er);
            return;
        } else {
            $(".js-aditionaluserinfo-form .js-error").text("");
            RenderController.emptyMessages();
            RenderController.successMessage("Succesfully updated!");
        }
    });
    $(".js-add-post").on("click", ".js-post", async (e)=>{
        $(e.currentTarget).hide();
        let title = $(".js-add-post").find("input[name='title']").val();
        let message = $(".js-add-post").find("textarea[name='message']").val();
        let picture = $(".js-add-post").find("input[name='imagelink']").val();
        let er;
        if (title == "" || message == "") {
            er = "Title and message must be filled in order to post."
        } 
        if (er == null) {
            er = Validators.maxLength(title) || Validators.maxLength(message) || Validators.maxLength(picture);
        }
        if (er == null) {
            er = await PostController.post(message, title, picture);
        }
        if (er != null) {
            RenderController.emptyMessages();
            $(".js-add-post .js-error").text(er);
        } else {
            $(".js-add-post .js-error").text("");
            RenderController.emptyMessages();
            RenderController.successMessage("Succesfully posted!");
        }
        $(e.currentTarget).show();
        currentUser.balance().then((data)=>{
            RenderController.renderBalance(Number(data).toFixed(2));
        });
    });
    $("body").on("click", ".js-post-like", async (e)=>{
        let $target = $(e.currentTarget);
        if (!($target.hasClass("notliked"))) {
            return;
        }
        $target.removeClass("notliked");
        let actionId = $target.closest(".js-post").attr("data-action-id");
        let userId = $target.closest(".js-post").attr("data-actor-id");
        let $curLikes = $target.closest(".js-post").find(".rn-post-like-count");
        let curLikes = Number($curLikes.text());

        let er = await PostController.like(actionId, userId);
        if (er != null) {
            RenderController.emptyMessages();
            $(".js-add-post .js-error").text(er);
            $target.addClass("notliked");
        } else {
            $(".js-add-post .js-error").text("");
            RenderController.emptyMessages();
            RenderController.successMessage("Succesfully liked!");
            curLikes++;
            $curLikes.text(curLikes);
            setTimeout(()=>{
                $(".js-reloadfeed").trigger("click");
            },500);
        }
    });
    $(".js-reloadfeed").on("click", ()=>{
        renderFeed(0, feedLimit);
    });
    $(".js-posts-more").on("click", ()=>{
        let offset = $(".posts .post").length;
        renderFeed(offset, feedLimit, true);
    });
    $("body").on("click", ".js-person .js-action", async (e)=>{
        let $tar = $(e.currentTarget);
        let personId = $tar.closest(".js-person").attr("data-user-id");
        let username = $tar.closest(".js-person").attr("data-username");
        let amIFollowing = $tar.hasClass("unfollow") ? true : false;
        RenderController.emptyMessages();
        if (amIFollowing == true) {
            $tar.removeClass("unfollow");
            $tar.addClass("follow");
            await FollowerController.unfollow(personId);
            RenderController.successMessage("You are not following " + username + " anymore.");
        } else {
            $tar.removeClass("follow");
            $tar.addClass("unfollow");
            await FollowerController.follow(personId);
            RenderController.successMessage("You are following " + username + ".");
        }
        renderFeed();
    });
}
function renderFeed(offset, limit, append = false) {
    $(".js-reloadfeed").hide();
    $(".js-posts-more").hide();
    PostController.myFeed(offset, limit).then(async (feed)=>{
        console.log(feed);
        if (append != true) {
            $(".rn-posts").text("");
        }
        await RenderController.renderFeed(".rn-posts", feed, currentUser.firebaseUser.uid);
        $(".js-reloadfeed").show();
        if (feed.length >= feedLimit) {
            $(".js-posts-more").show();
        }
    });
}
function renderUsers(currentUser) {
    DatabaseController.getAllUsers(currentUser.firebaseUser.uid).then((data)=>{
        RenderController.rednerUsers(".rn-repole", data);
    });
}

function eventUnlogged() {
    //login
    $("body").on("click", ".js-loginform .js-login", async (e)=>{
        let req = new LoginRequest();
        req.username = $(".js-loginform").find("input[name='username']").val();
        req.password = $(".js-loginform").find("input[name='password']").val();
        let er = await UserController.signIn(req);
        if (er != null) {
            $(".js-loginform .js-error").text(er);
            return;
        } else {
            $(".js-loginform .js-error").text("");
            Router.feed();
        }
    });
    //create use
    $("body").on("click", ".js-registerform .js-register", async (e)=>{
        let req = new LoginRequest();
        let username = $(".js-registerform").find("input[name='username']").val();
        let password = $(".js-registerform").find("input[name='password']").val();
        let passComf = $(".js-registerform").find("input[name='passwordconf']").val();

        //check if filled
        if (username == "" || password == "" || passComf == "")  {
            $(".js-registerform .js-error").text("Please fill all fields.");
            return;
        } else if (username.indexOf("@") == -1) {
            $(".js-registerform .js-error").text("Email must include @."); 
            return;
        } else if (Helpers.ostSafeName(username.split("@")[0]).length > 20 || Helpers.ostSafeName(username.split("@")[0]).length < 3) {
            $(".js-registerform .js-error").text("Part before @ must must be a minimum of 3 characters and maximum of 20 characters withou special characters."); 
            return;
        } else if (password != passComf) {
            $(".js-registerform .js-error").text("Passwords must match.");
            return;
        } else {
            $(".js-registerform .js-error").text("");
        }

        req.username = username;
        req.password = password;

        let er = await UserController.createFirebaseUser(req);
        console.log(er);
        if (er != null) {
            $(".js-registerform .js-error").text(er);
            return;
        } else {
            $(".js-registerform .js-error").text("");
            Router.feed();
        }
    });
}

function unbindEvents() {
    $("body").off("click");
}
